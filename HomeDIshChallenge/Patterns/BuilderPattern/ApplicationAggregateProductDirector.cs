﻿using HomeDIshChallenge.Controllers.Dtos;
using HomeDIshChallenge.Models;

namespace HomeDIshChallenge.Patterns.BuilderPattern
{
    public class ApplicationAggregateProductDirector
    {
        private readonly IBuilder<AggregateProduct, ApplicationAggregateProduct> _builder;

        public ApplicationAggregateProductDirector(IBuilder<AggregateProduct, ApplicationAggregateProduct> builder)
        {
            _builder = builder;
        }

        public void BuildApplicationAggregateProduct(AggregateProduct input)
        {
            _builder.CreateApplicationAggregateProduct(input);
            _builder.BuildApplicationOffers();
        }
    }
}