﻿namespace HomeDIshChallenge.Patterns.BuilderPattern
{
    public interface IBuilder<TBuildFrom, TTarget>
    {
        void CreateApplicationAggregateProduct(TBuildFrom input);
        void BuildApplicationOffers();
        TTarget GetAggregateProduct();
    }
}