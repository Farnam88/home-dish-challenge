﻿using System.Linq;
using HomeDIshChallenge.Controllers.Dtos;
using HomeDIshChallenge.Models;
using HomeDIshChallenge.Patterns.AdapterPattern;

namespace HomeDIshChallenge.Patterns.BuilderPattern
{
    public class ConcreteBuilder : IBuilder<AggregateProduct, ApplicationAggregateProduct>
    {
        private ApplicationAggregateProduct _aggregateProduct;
        private readonly IAdapter<AggregateProduct, ApplicationAggregateProduct> _adapter;

        public ConcreteBuilder(IAdapter<AggregateProduct, ApplicationAggregateProduct> adapter)
        {
            _adapter = adapter;
        }

        public void CreateApplicationAggregateProduct(AggregateProduct input)
        {
            _aggregateProduct = _adapter.GetTargetObject(input);
        }

        public void BuildApplicationOffers()
        {
            foreach (var applicationOffer in _aggregateProduct.SpecialOffers)
            {
                foreach (var specialProduct in applicationOffer.Products)
                {
                    var relatedProduct = _aggregateProduct.Products.FirstOrDefault(f => f.Equals(specialProduct));
                    specialProduct.SetProductBasketDetails(relatedProduct);
                }
            }
        }

        public ApplicationAggregateProduct GetAggregateProduct()
        {
            return _aggregateProduct;
        }
    }
}