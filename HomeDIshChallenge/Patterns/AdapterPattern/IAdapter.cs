﻿namespace HomeDIshChallenge.Patterns.AdapterPattern
{
    public interface IAdapter<TAdaptee,TAdapted>
    {
        TAdapted GetTargetObject(TAdaptee input);
    }
}