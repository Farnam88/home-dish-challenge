﻿using System.Linq;
using HomeDIshChallenge.Controllers.Dtos;
using HomeDIshChallenge.Models;

namespace HomeDIshChallenge.Patterns.AdapterPattern
{
    public class AggregateProductAdapter : IAdapter<AggregateProduct, ApplicationAggregateProduct>
    {
        public ApplicationAggregateProduct GetTargetObject(AggregateProduct input)
        {
            return new ApplicationAggregateProduct
            {
                Products = input.Products.Select(s => new ApplicationProduct(s.Name,
                    s.Price,
                    s.Quantity)).ToHashSet(),
                SpecialOffers = input.Specials.Select(s => new ApplicationOffer
                {
                    Total = s.Total,
                    Products = s.Products.Select(d => new ApplicationSpecialProduct(d.Name,
                        d.Quantity)).ToHashSet()
                }).ToHashSet()
            };
        }
    }
}