﻿using System;
using HomeDIshChallenge.Controllers.Dtos;
using HomeDIshChallenge.Models;
using HomeDIshChallenge.Patterns.BuilderPattern;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HomeDIshChallenge.Controllers
{
    [ApiController]
    public class HomeDishController : ControllerBase
    {
        private readonly ILogger<HomeDishController> _logger;
        private readonly IBuilder<AggregateProduct, ApplicationAggregateProduct> _builder;
        private readonly ApplicationAggregateProductDirector _director;

        public HomeDishController(ILogger<HomeDishController> logger
            , IBuilder<AggregateProduct, ApplicationAggregateProduct> builder)
        {
            _logger = logger;
            _builder = builder;
            _director = new ApplicationAggregateProductDirector(_builder);
        }

        [HttpPost("total")]
        public IActionResult Post(AggregateProduct input)
        {
            try
            {
                _director.BuildApplicationAggregateProduct(input);
                ApplicationAggregateProduct applicationAggregateProduct = _builder.GetAggregateProduct();
                return Ok(applicationAggregateProduct.BasketMinimumGrandTotal());
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message, new {e.StackTrace, e.Source});
                return Content(e.Message);
            }
        }
    }
}