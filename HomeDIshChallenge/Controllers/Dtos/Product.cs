﻿namespace HomeDIshChallenge.Controllers.Dtos
{
    public class Product : BaseProduct
    {
        public double Price { get; set; }
    }
}