﻿using System.Collections.Generic;

namespace HomeDIshChallenge.Controllers.Dtos
{
    public class AggregateProduct
    {
        public List<Product> Products { get; set; }
        public List<Special> Specials { get; set; }
    }
}