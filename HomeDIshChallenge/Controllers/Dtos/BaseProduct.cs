﻿using System.ComponentModel.DataAnnotations;

namespace HomeDIshChallenge.Controllers.Dtos
{
    public abstract class BaseProduct
    {
        public string Name { get; set; }
        [Required] public int Quantity { get; set; }
    }
}