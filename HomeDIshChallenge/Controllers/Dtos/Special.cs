﻿using System.Collections.Generic;

namespace HomeDIshChallenge.Controllers.Dtos
{
    public class Special
    {
        public IEnumerable<SpecialProduct> Products { get; set; }
        public double Total { get; set; }
    }
}