﻿using System.Collections.Generic;
using System.Linq;

namespace HomeDIshChallenge.Models
{
    public class ApplicationAggregateProduct
    {
        public HashSet<ApplicationProduct> Products { get; set; }

        public HashSet<ApplicationOffer> SpecialOffers { get; set; }

        private double BasketTotalPrice
        {
            get { return Products.Sum(s => s.TotalPrice); }
        }

        public double BasketMinimumGrandTotal()
        {
            return BasketTotalPrice - SpecialOffers.Max(m => m.BasketNetDiscount);
        }
    }
}