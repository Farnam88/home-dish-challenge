﻿using System.Collections.Generic;
using System.Linq;

namespace HomeDIshChallenge.Models
{
    public class ApplicationOffer
    {
        public HashSet<ApplicationSpecialProduct> Products { get; set; }
        public double Total { get; set; }

        public int MaximumPossibleApplicableSpecialOffer
        {
            get { return Products.Min(m => m.PossibleAppliedOfferOnBasket); }
        }

        public double BasketNetDiscount
        {
            get
            {
                var applicableSpecialProductsTotalPrice =
                    Products.Sum(s => s.SpecialProductTotalPrice * MaximumPossibleApplicableSpecialOffer);
                var specialOfferPrice = Total * MaximumPossibleApplicableSpecialOffer;
                return applicableSpecialProductsTotalPrice - specialOfferPrice;
            }
        }
    }
}