﻿using System;

namespace HomeDIshChallenge.Models
{
    public class ApplicationProduct : IEquatable<ApplicationSpecialProduct>
    {
        #region + Members and Constructor

        private readonly string _name;
        private readonly double _price;
        private readonly int _quantity;

        public ApplicationProduct(string name, double price, int quantity)
        {
            _name = name;
            _price = price;
            _quantity = quantity;
        }

        public string Name => _name;
        public double Price => _price;
        public int Quantity => _quantity;

        public double TotalPrice
        {
            get { return _quantity * _price; }
        }

        #endregion

        #region + Equality

        public bool Equals(ApplicationSpecialProduct other)
        {
            return other != null && other.Name == this._name;
        }

        public override int GetHashCode()
        {
            return _name.GetHashCode();
        }

        #endregion
    }
}