﻿using System;

namespace HomeDIshChallenge.Models
{
    public class ApplicationSpecialProduct : IEquatable<ApplicationProduct>
    {
        #region + Members and Constructor

        private readonly string _name;
        private double _basketProductPrice;

        public int PossibleAppliedOfferOnBasket => _basketProductQuantity / _quantity;

        public double SpecialProductTotalPrice => _quantity * _basketProductPrice;

        private int _basketProductQuantity;
        private readonly int _quantity;

        public ApplicationSpecialProduct(string name, int quantity)
        {
            _name = name;
            _quantity = quantity;
        }

        public string Name => _name;
        public int Quantity => _quantity;

        #endregion

        #region + Equality

        public bool Equals(ApplicationProduct other)
        {
            return other != null && other.Name == this._name;
        }

        public override int GetHashCode()
        {
            return _name.GetHashCode();
        }

        #endregion

        #region + Functions

        public void SetProductBasketDetails(ApplicationProduct product)
        {
            if (product == null)
                return;
            _basketProductQuantity = product.Quantity;
            _basketProductPrice = product.Price;
        }

        #endregion
    }
}