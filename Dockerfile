FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /app
COPY HomeDIshChallenge/*.csproj ./
RUN dotnet restore
COPY ./HomeDIshChallenge ./
RUN dotnet publish -c Release -o out
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
EXPOSE 80
COPY --from=build /app/out .
ENTRYPOINT ["dotnet","HomeDIshChallenge.dll", "--environment=Production"]